package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static java.text.DateFormat.getDateTimeInstance;

public class Events {

    private final String USER_AGENT = "Mozilla/5.0";

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter Artist Name: ");
        String artistName = null;
        try {
            artistName = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (artistName != null && !artistName.isEmpty()) {
            Events events = new Events();
            try {
                events.getEvents(artistName);
                events.parseJson(artistName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void parseJson(String artistName) throws Exception {
        String fileName = String.format("%s.txt", artistName);
        // read the json file
        FileReader reader = new FileReader(fileName);

        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);

        // take the elements of the json array
        for(int i=1; i<=jsonArray.size(); i++){
            System.out.println("Event " + i + " from " + jsonArray.size());
            JSONObject event = (JSONObject) jsonArray.get(i-1);
            System.out.println("Date and Time: " + this.formatDate((String)event.get("datetime")));
            System.out.println("Title: " + event.get("title"));
            JSONObject venue = (JSONObject) event.get("venue");
            System.out.println("City: " + venue.get("city"));
            System.out.println("Country: " + venue.get("country"));
            System.out.println("Link: " + event.get("facebook_rsvp_url"));
            System.out.println();
        }
    }

    private String formatDate(String dateString) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
        Date date =  df.parse(dateString);
        Locale locale = new Locale(System.getProperty("user.language"), System.getProperty("user.country"));
        DateFormat f = getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);
        return f.format(date);
    }

    private void getEvents(String artistName) throws Exception {

        String url = String.format("http://api.bandsintown.com/artists/%s/events.json?api_version=2.0&app_id=YOUR_APP_ID", artistName);

        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        connection.setRequestMethod("GET");

        //add request header
        connection.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = connection.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

        saveToFile(artistName, response);
    }

    private void saveToFile(String artistName, StringBuffer response) throws IOException {
        String fileName = String.format("%s.txt", artistName);
        File file = new File(fileName);
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        try {
            writer.write(response.toString());
        } finally {
            writer.close();
        }
    }
}
